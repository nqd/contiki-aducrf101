/**
 *****************************************************************************
   @file     WdtLib.h
   @brief    Set of watchdog Timer peripheral functions.
   @version  V0.1
   @author   PAD CSE group
   @date     January 2012 

All files for ADuCRF101 provided by ADI, including this file, are
provided  as is without warranty of any kind, either expressed or implied.
The user assumes any and all risk from the use of this code.
It is the responsibility of the person integrating this code into an application
to ensure that the resulting application performs as required and is safe.

*****************************************************************************/

#ifndef WDT_LIB_H
#define WDT_LIB_H

#include "contiki.h"
#include "platform-conf.h"
#include "ADuCRF101.h"
#include "aducrf101-contiki.h"
#include "aducrf101-include.h"

int WdtCfg(int iPre, int iInt, int iPd);
int WdtGo(int iEnable);
int WdtLd(int iTld);
int WdtVal(void);
int WdtSta(void);
int WdtClrInt(int iSource);

#endif