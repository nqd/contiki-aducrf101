/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * Nguyen Quoc Dinh, nqdinh@ubisen.com
 * Jan, 2015
 */
#include "contiki.h"
#include "clock.h"
#include "dev/DioLib.h"
#include "dev/AdcLib.h"
#include "dev/adc-sensor.h"

#include <stdint.h>

/*---------------------------------------------------------------------------*/
// ADCCFG_CHSEL_ADC0, ADCCFG_CHSEL_ADC1, ADCCFG_CHSEL_ADC2, ADCCFG_CHSEL_ADC3
// ADCCFG_CHSEL_ADC4 , ADCCFG_CHSEL_ADC5, ADCCFG_CHSEL_TEMP
// ADCCFG_CHSEL_VBATDIV4, ADCCFG_CHSEL_LVDDDIV2
static int
value(int type)
{
  uint8_t channel;
  uint16_t res;

  switch(type) {
  case ADC_SENSOR_TEMP:
    channel = ADCCFG_CHSEL_TEMP;
    // ADC power up sequence complete
    AdcCfg(channel, ADCCFG_REF_INTERNAL125V, ADCCFG_CLK_FCOREDIV16, ADCCFG_ACQ_8 );
    break;
  case ADC_SENSOR_CHANNEL0:
    channel = ADCCFG_CHSEL_ADC0;
    // ADC power up sequence complete
    AdcCfg(channel, ADCCFG_REF_LVDD, ADCCFG_CLK_FCOREDIV16, ADCCFG_ACQ_8 );
    break;
  default:
    return 0;
  }

  // start software conversion
  AdcCnv(ADCCON_MOD_SOFT, ADCCON_START_EN);
  // wait for adc conversion to complete
  while (!AdcSta()) {};
  res = AdcRd(0);

  return res;
}
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
  switch(type) {
  case SENSORS_HW_INIT:
    // ADC initialisation
    AdcPd(ADCCON_ENABLE_EN);        // Enable ADC and reference
    clock_delay_usec(2500);         // wait ~2.5ms
    AdcInit(ADCCON_REFBUF_EN, ADCCON_IEN_DIS, ADCCON_DMA_DIS); // Enable reference buffer 
    clock_delay_usec(1950);         // wait ~1.95ms
    break;
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
static int
status(int type)
{
  return 1;
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(adc_sensor, ADC_SENSOR, value, configure, status);
